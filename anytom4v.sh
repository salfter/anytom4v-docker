#!/usr/bin/env bash

help()
{
cat <<EOF >&2
Usage: $0 [options] infile

options: -o|--output        specify output file (default: infile.m2ts)
         -c|--crop w:h:x:y  manually specify cropping dimensions
         		    (width:height:X, Y of upper left)
         -a|--auto-crop     crop automatically (default)
         -n|--no-crop       disable cropping
	 -f|--framerate     override detected framerate
	 		    (acceptable values: NTSC, NTSCFilm, PAL)
	 -g|--grayscale	    throw out color information
	 -l|--detelecine    detelecine interlaced source
	 -m|--decomb	    decomb interlaced source
	 -d|--decimate n    remove every nth frame
	 -k|--skip n	    skip first n seconds of source
	 -i|--omit n	    omit last n seconds of source
	 -s|--denoise	    apply denoise filter
	 		    (acceptable values: weak, medium, strong)
	 -w|--width         output video width (default: same as input)
	 -b|--bitrate       video bitrate for 2-pass encoding, in kbps
	 		    use "auto" to calculate based on framesize
7	 		    (default: 1-pass constant-quality encoding)
	 -p|--pal2ntsc      slow down PAL-telecined video to NTSCFilm
	 -t|--subtitle	    copy subtitles from source to destination
	 -r|--audio-format  override audio format (default: same as source)
EOF
}

encoder="ffmpeg"
copysubs="-sn"

while getopts "o:c:anf:hs:w:b:glmtd:k:i:pr:" opt; do
  case $opt in
    o) outfile="$OPTARG";;
    c) crop="--crop=$OPTARG";;
    a) crop="--loose-crop=15 --loose-anamorphic --modulus=16";;
    n) crop="--crop=0:0:0:0";;
    f) framerate_override="$OPTARG";;
    h) help; exit 1;;
    g) grayscale="--grayscale";;
    l) detelecine="--detelecine";;
    m) decomb="--decomb";;
    s) denoise="$OPTARG";;
    w) encwidth="$OPTARG";;
    b) vbr="$OPTARG";;
    p) p2n='-filter:v setpts=1.042708*PTS -r:v 24000/1001 -filter:a asetrate=46034,aresample=48000';;
    t) copysubs="-c:s copy -scodec mov_text";;
    d) decimate="$OPTARG";;
    k) skip="$OPTARG";;
    i) omit="$OPTARG";;
    r) af_override="$OPTARG";;
  esac
done
shift "$((OPTIND-1))"

if [ "$crop" == "" ]
then
  crop="--loose-crop=15 --loose-anamorphic --modulus=16"
fi

infile="$1"
if [ "$outfile" == "" ]
then
  outfile="${infile}.m2ts"
fi

TMPDIR=/var/tmp/anytom2ts.$$
mkdir -p $TMPDIR
# warning: tab embedded in second sed replacement, because BSD sed doesn't grok "\t"
cat <<EOF | sed "s/\$/\r/;s/ /	/g" >$TMPDIR/mediainfo-params 
Video;%Width% %Height% %DisplayAspectRatio% %Format% %FrameRate% %Duration% 
Audio;%Format% %Channels% %SamplingRate%
Text;%Format% %StreamKindID%
EOF
eval `mediainfo --Inform=file://$TMPDIR/mediainfo-params "$1" | tr -d "\n" | awk 'BEGIN {FS="\t"} {printf("width=\"%s\"\nheight=\"%s\"\naspect=\"%s\"\nvcodec=\"%s\"\nframerate=\"%s\"\nduration=\"%s\"\nacodec=\"%s\"\nchannels=\"%s\"\nsamplingrate=\"%s\"\nsubs=\"%s\"\nsub_track_id=\"%s\"\n",$1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11);}'`

if [ "$p2n" != "" ]
then
  new_rate=`echo $samplingrate | awk '{print 0.959041*$1}'`
  p2n="`echo $p2n | sed "s/46034/$new_rate/"`"
fi

if [ \( "$decomb" != "" \) -a \( "$framerate" == "23.976" \) ]
then
  echo "Invalid source framerate ($framerate) for decomb"
  exit 1
fi
if [ \( "$detelecine" != "" \) -a \( "$framerate" != "29.970" \) -a \( "$encoder" == "handbrake" \) ]
then
  echo "Invalid source framerate ($framerate) for detelecine"
  exit 1
fi
if [ \( "$decimate" != "" \) ]
then
  framerate=`echo "scale=3; $framerate*($decimate-1)/$decimate" | bc`
  decimate="-vf decimate=cycle=$decimate,setpts=N/$framerate/TB"
fi
if [ "$framerate_override" != "" ]
then
  case "$framerate_override" in
    "")		;;
    "NTSC")	framerate=29.970;;
    "NTSCFilm")	framerate=23.976;;
    "PAL")	framerate=25.000;;
    *)		help; exit 1;;
  esac
fi
if [ "$denoise" != "" ]
then
  case "$denoise" in
    "weak")	denoise="--denoise weak";;
    "medium")	denoise="--denoise medium";;
    "strong")	denoise="--denoise strong";;
    *)		help; exit 1;;
  esac
fi

if [ "$encoder" == "handbrake" ]
then
case "$framerate" in
  "23.976")	hbfr=23.976;;
  "24.000")	hbfr=24;;
  "25.000")	hbfr=25;;
  "29.970")	hbfr=29.97;;
  *)		echo Don\'t know how to handle $framerate fps; exit 1;;
esac
fi

if [ "$encwidth" != "" ]
then
  encwidth="--width=$encwidth"
fi

if [ \( $width -gt 720 \) -o \( $height -gt 576 \) ]
then
  preset="HQ 1080p30 Surround"
  if [ $height -gt 720 ]
  then
    qual="--quality 23"
    ffmpeg_qual=23
  else
    qual="--quality 21"
    ffmpeg_qual=21
  fi
#  encopts="b-adapt=2:rc-lookahead=50:me=umh:merange=32:direct=auto:psy-rd=1|0.15:vbv-maxrate=6000"
else
  if [ $height -gt 480 ]
  then
    preset="HQ 576p25 Surround"
  else
    preset="HQ 480p30 Surround"
  fi
  qual="--quality 21"
  ffmpeg_qual=21
#  encopts="rc-lookahead=10:8x8dct=0:me=umh:merange=32:direct=auto:b-adapt=2:psy-rd=1|0.15:vbv-maxrate=3000"
fi

if [ "$vbr" == "auto" ]
then
  vbr=`echo $width \* $height \* 1.75 / 1000 | bc`
fi

if [ "$vbr" != "" ]
then
  qual="--turbo --two-pass --vb $vbr"
fi

if [ "$acodec" == "AC-3" ]
then
  acodec=AC3
fi

#if [ \( \( "$acodec" == "AC3" \) -a \( "$channels" -eq 6 \) \) -o \( \( "$acodec" == "AAC" \) -a \( \( $channels -eq 1 \) -o \( $channels -eq 2 \) \) \)  ]
#then
#  audio_opts="--audio 1 --aencoder copy"
#else
#  if [ "$channels" == 1 ]
#  then
#    acodec=AAC
#    audio_opts="--audio 1 --aencoder ffaac --ab 64 --mixdown mono --arate 48"
#  fi
#  if [ "$channels" == 2 ]
#  then
#    acodec=AAC
#    audio_opts="--audio 1 --aencoder ffaac --ab 128 --mixdown stereo --arate 48"
#  fi
#  if [ \( "$channels" == 5 \) -o  \( "$channels" == 6 \) -o \( "$channels" == 8 \) -o \( "$channels" == "8 / 6" \) ]
#  then
#    acodec=AC3
#    audio_opts="--audio 1 --aencoder ffac3 --ab 448 --mixdown 6ch --arate 48"
#  fi
#fi

#if [ "$acodec" == "AAC" ]
#then
#  audio_opts="--audio 1 --aencoder copy"
#else
  case "$channels" in
  1)
    audio_opts="-c:a libfdk_aac -profile:a aac_he -b:a 64k"
    ffmpeg_audio_bitrate=64
    ;;
  2)
    audio_opts="-c:a libfdk_aac -profile:a aac_he -b:a 128k"
    ffmpeg_audio_bitrate=128
    ;;
  [456])
    audio_opts="-c:a libfdk_aac -profile:a aac_he -b:a 192k"
    ffmpeg_audio_bitrate=192
    ;;
  8)
    audio_opts="-c:a libfdk_aac -profile:a aac_he -b:a 256k"
    ffmpeg_audio_bitrate=256
    ;;
  "8 / 6")
    audio_opts="-c:a libfdk_aac -profile:a aac_he -b:a 256k"
    ffmpeg_audio_bitrate=256
    ;;
  esac
#fi
 
  case "$af_override" in
  1)
    audio_opts="-ac 1 -c:a libfdk_aac -profile:a aac_he -b:a 64k"
    ffmpeg_audio_bitrate=64
    ;;
  2)
    audio_opts="-ac 2 -c:a libfdk_aac -profile:a aac_he -b:a 128k"
    ffmpeg_audio_bitrate=128
    ;;
  5.1)
    audio_opts="-ac 5.1 -c:a libfdk_aac -profile:a aac_he -b:a 192k"
    ffmpeg_audio_bitrate=192
    ;;
  7.1)
    audio_opts="-ac 7.1 -c:a libfdk_aac -profile:a aac_he -b:a 256k"
    ffmpeg_audio_bitrate=256
    ;;
  esac
  
if [ "$audio_opts" == "" ]
then
  echo "Unknown audio configuration ($acodec, $channels channels)"
  exit 1
fi

if [ \( "$detelecine" != "" \) -a \( "$decomb" != "" \) ]
then
  echo "Detelecine and decomb are mutually-exclusive options"
  exit 1
fi

if [ \( "$encoder" == "ffmpeg" \) -a \( "$decomb" != "" \) ]
then
  echo "Decomb not supported by FFmpeg"
  exit 1
fi

if [ \( "$encoder" == "ffmpeg" \) -a \( "$denoise" != "" \) ]
then
  echo "Denoise not supported by FFmpeg"
  exit 1
fi

#if [ "$subtitle" != "" ]
#then
#  subopts="--subtitle $subtitle --subtitle-burn"
#fi

if [ "$subs" == "SSA" ]
then
  subopts="--subtitle `echo $sub_track_id + 1 | bc` --subtitle-burn `echo $sub_track_id + 1 | bc`"
fi

if [ "$omit" != "" ]
then
  omit=`echo "scale=3; $duration/1000-$omit" | bc`
fi

if [ "$encoder" == "handbrake" ]
then
  # 16 Mar 17: improved auto-crop
  if [ "$crop" == "--loose-crop=15 --loose-anamorphic --modulus=16" ]
  then
    eval $(ffmpeg -ss 300 -t 60 -i "$infile" -vf cropdetect=24:16:0 -f null - 2>&1 | grep -o crop=.* | sort -bh | uniq -c | sort -bh | tail -n1 | grep -o crop=.* | sed "s/crop=/cw=/;s/:/ ch=/;s/:/ cl=/;s/:/ ct=/" )
    cr=$(echo $width-$cw-$cl | bc)
    cb=$(echo $height-$ch-$ct | bc)
    crop="--crop=$ct:$cb:$cl:$cr"
  fi
#HandBrakeCLI --input "$infile" --output "$outfile" \
#	     --preset "$preset" "$encwidth" "$crop" \
#	     --encoder x264 --x264-preset medium  --encopts opencl=1 $qual --cfr --rate $hbfr \
#	     $audio_opts $grayscale $detelecine $decomb $denoise $subopts && \

HandBrakeCLI --input "$infile" --output "$outfile" \
	     --preset "$preset" "$encwidth" "$crop" \
	     --encoder x264 --x264-preset medium  $qual --cfr --rate $hbfr \
	     $audio_opts $grayscale $detelecine $decomb $denoise $subopts 

#mv "$TMPDIR/tmp.m4v" "$outfile" && \
fi

if [ "$encoder" == "ffmpeg" ]
then
  if [ "$crop" == "--loose-crop=15 --loose-anamorphic --modulus=16" ]
  then
    crop="-vf $(ffmpeg -ss 300 -t 60 -i "$infile" -vf cropdetect=24:16:0 -f null - 2>&1 | grep -o crop=.* | sort -bh | uniq -c | sort -bh | tail -n1 | grep -o crop=.*)"
  else
  if [ "$crop" == "--crop=0:0:0:0" ]
  then
    crop=""
  else
    crop="-vf `echo $crop | sed "s/--//"`"
  fi
  fi
  
  if [ "$detelecine" != "" ]
  then
    detelecine="-vf dejudder,fps=ntsc,fieldmatch,decimate"
  fi

  qual="`echo $qual | sed "s/--quality //"`"  

  if [ \( "$detelecine" != "" \) -a \( "$crop" != "" \) ]
  then
    crop="`echo $crop | sed "s/-vf /,/"`"
  fi

  if [ \( "$skip" != "" \) -o \( "$omit" != "" \) ]
  then
    skip_params="-accurate_seek "
    if [ "$skip" != "" ]
    then
      skip_params="$skip_params -ss $skip "
    fi
    if [ "$omit" != "" ]
    then
      skip_params="$skip_params -to $omit "
    fi
  fi
  
  if [ \( "$crop" != "" \) -a \( "$p2n" != "" \) ]
  then
    crop=$(echo "$crop" | sed "s/-vf crop=//")
    p2n=$(echo "$p2n" | sed "s/-filter:v /-filter:v crop=$crop,/")
    crop=""
  fi

# 16 Sep 19: force 8-bit video with pix_fmt
ffmpeg $skip_params -i "$infile" -pix_fmt yuv420p $p2n \
  -c:v libx264 -preset slow -tune film -crf ${ffmpeg_qual} \
  ${detelecine}${crop} ${decimate} \
  ${audio_opts} \
  ${copysubs} \
  "$outfile"
#  -c:a libfdk_aac -profile:a aac_he -b:a ${ffmpeg_audio_bitrate}k \
fi

rm -r $TMPDIR

anytom4v
========

This is a shell script I developed a while back, first to automate HandBrake
from the command line, then to automate ffmpeg in a similar manner.  As it 
stands now, it accepts input in whatever formats ffmpeg accepts and
transcodes to H.264 video with aggressive compression but still decent
(IMHO) quality and HE-AAC audio.  Simple options to do things like
detelecine, PAL-to-NTSC conversion, and inclusion of subtitles (they're
normally excluded) are available; run with -h to see what you can do.

Now the script is containerized, and can be invoked with something like:

```docker run -it --rm -v /mnt/files:/mnt/files salfter/anytom4v -o /mnt/files/video.m4v /mnt/files/src_video.mkv```

Development is over at https://gitlab.com/salfter/anytom4v-docker.

**Update** (9 Jun 22): Switched the base container from Debian to Gentoo,
as the script was developed on Gentoo and uses options that apparently
aren't in Debian builds of ffmpeg.

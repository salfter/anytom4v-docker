FROM gentoo/stage3
RUN emaint sync -a && MAKEOPTS="-j12" FEATURES="parallel-fetch" EMERGE_DEFAULT_OPTS="--autounmask-write --quiet-build=y" USE="x264 fdk x265 cpudetection libass openssl vpx -introspection" emerge -v ffmpeg mediainfo && rm -r /var/db/repos/gentoo /var/cache/distfiles
COPY anytom4v.sh /usr/bin/
ENTRYPOINT ["/usr/bin/anytom4v.sh"]
CMD ["-h"]


